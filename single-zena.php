<?php
// NET.HR -> Single article
get_header('zena-net');
?>
    <div class="container single single-zena cf">

        <?php
            while ( have_posts() ) {
                the_post(); ?>

                <div class="single-header">

                    <?php if( has_post_thumbnail() ) { ?>
                        <div class="featured-img thumb">
                            <?php the_post_thumbnail( 'single-raw' ); ?>
                        </div>
                    <?php }
					the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_header' ) );
                    ?>

                    <?php
                    $titles = get_post_meta( get_the_ID(), 'extra_titles', true );  ?>
                    <h3 class="overtitle"><?php
                        if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
                            echo esc_html( $titles['over_title'] );
                        } ?>
                    </h3>

                    <h1 class="title"><?php the_title(); ?></h1>
                </div>

                <div class="page-grid single-article">

                    <div class="metabox">
                        Piše: <?php coauthors(); ?> /
                        Vrijeme: <?php the_time(); ?> /
                        Rubrika: <?php the_terms( get_the_ID(), 'zena-cat' ) ?>
                        <?php if( nethr_get_photographer() ) {
                            echo esc_html( " / ".nethr_get_photographer() ); } ?>
                    </div>

                    <?php get_template_part('templates/new-share'); ?>

                    <div class="article-content" id="__xclaimwords_wrapper">
                        <?php the_content(); ?>

                        <div class="metabox">
                            Piše: <?php coauthors(); ?> /
                            Vrijeme: <?php the_time(); ?> /
                            Rubrika: <?php the_terms( get_the_ID(), 'zena-cat' ) ?>
                        </div>

                    </div>

                    <div class="tags">
                        <?php the_tags( '', ' ', '' ); ?>
                    </div>

                    <?php
                    if( get_the_terms( get_the_ID(), 'zena-cat' )[0]->name == 'Pitaj frajera' && $email = get_option( 'nethr_zena_frajer' )  ) {
                        if ( isset($_POST['_wpnonce']) && $_POST['_wpnonce'] && wp_verify_nonce( $_POST['_wpnonce'], 'frajer' ) ) {
                            //process submit
                            if ( isset( $_POST['name'] ) && $_POST['name'] && isset( $_POST['question'] ) && $_POST['question'] ) {
                                $message = 'Korisnik: ' . sanitize_text_field( $_POST['name'] ). "\n" . sanitize_text_field( $_POST['question'] );
                                wp_mail( $email, 'Pitaj frajer - pitanje', $message );
                                ?>
                                <div id="mirpar_sent">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="https://adriaticmedianethr.files.wordpress.com/2016/07/miropar_sent.png"
                                             width="640" height="185"/>
                                    </a>
                                </div>
                                <?php
                            }
                        }
                        else {
                            ?>
                            <div class="pitaj-frajera-form" id="frajer">
                                <div class="form-head">
                                    <img
                                        src="https://adriaticmedianethr.files.wordpress.com/2016/07/miropar23.png"
                                        width="85"/>
                                    <div class="text">
                                        <h3 class="overtitle">Miro Par
                                            odgovara na vaša pitanja</h3>
                                        <h1 class="title">Pitaj frajera</h1>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <form id="miro-par-wunderbar"
                                          method="post"
                                          action="<?php the_permalink() ?>#frajer">
                                        <input type="text" name="name"
                                               placeholder="Tvoje ime..."
                                               required>
											<textarea name="question"
                                                      placeholder="Tvoje pitanje..."
                                                      required></textarea>
                                        <?php wp_nonce_field( 'frajer' ) ?>
                                        <input type="submit" class="button"
                                               value="Pošalji">
                                    </form>
                                </div>
                            </div>

                            <?php
                        }
                    } ?>

                    <?php get_template_part( 'templates/share' ); ?>

                    <?php the_widget('Nethr_Facebook_Comments_Widget') ?>

                </div>

            <?php
            } //endwhile
        ?>

    </div>

<?php
$post_id = get_the_ID();
$read_more = wp_cache_get( 'read_more_' . $post_id, 'nethr_mobile' );
if ( !$read_more ) {
    ob_start();
    ?>
    <h2 class="section-title head">Pročitaj i ovo</h2>
    <div class="read-more">
        <?php
        $args = array(
            'size' => 5,
        );
        if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
	        $posts = Jetpack_RelatedPosts::init_raw()->get_for_post_id( $post_id, $args );

	        foreach ( $posts as $post ) {
		        $id = $post['id'];
		        ?>
		        <article class="article-3 <?php echo esc_html( nethr_category_color() ) ?> cf">
			        <a href="<?php the_permalink( $id ); ?>">
				        <div class="article-text">
					        <div class="thumb"><?php echo wp_kses_post( get_the_post_thumbnail( $id, 'article-3_2x' ) ); ?></div>
					        <h2 class="overtitle">
						        <?php
						        $titles = get_post_meta( $id, 'extra_titles', true );
						        if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
							        echo esc_html( $titles['over_title'] );
						        }
						        else {
							        echo "Žena";
						        }
						        ?>
					        </h2>
					        <h1 class="title"><?php echo esc_html( get_the_title( $id ) ); ?></h1>
				        </div>
			        </a>
		        </article>
		        <?php
	        }
        }
        ?>
    </div>
    <?php
    $read_more = ob_get_clean();
    wp_cache_set( 'read_more_' . $post_id, $read_more, 'nethr_mobile', 4 * HOUR_IN_SECONDS );
}
echo $read_more;
?>

<?php the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v1' ) ) ?>

<?php
get_footer();
