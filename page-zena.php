<?php
get_header('zena-net');
?>
    <div class="container category cf">

        <div class="page-grid">
            <?php
            global $post;
            $q = z_get_zone_query( 'zena', array( 'posts_per_page' => 10 ));
            while ( $q->have_posts() ) {
                $q->the_post();
                if ( 0 == $q->current_post ) {
                    get_template_part( 'templates/articles/article-1' );
                    the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_header' ) );
                }
                else if ( 4 == $q->current_post || 8 == $q->current_post ) {
                    get_template_part( 'templates/articles/article-2' );
                }
                else {
                    get_template_part( 'templates/articles/article-3' );
                }
            }

            the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v1' ) );

            ?>
        </div>

            <section class="feed cf">
                <?php
                $feed = wp_cache_get( 'nethr_zena_mobile_feed', 'nethr_mobile' );
                if ( !$feed ) {
                	ob_start();
	                //main feed
	                $posts_in_zone = z_get_posts_in_zone( 'zena', array( 'fields' => 'ids' ) );

	                $q = new WP_Query(
		                array(
			                'post_type'           => array( 'zena' ),
			                'posts_per_page'      => 20,
			                'post_status'         => 'publish',
			                'no_found_rows'       => true,
			                'ignore_sticky_posts' => true,
			                'post__not_in'        => $posts_in_zone
		                )
	                );
	                if ( $q->have_posts() ) {
		                while ( $q->have_posts() ) {
			                $q->the_post();
			                get_template_part( 'templates/articles/article-3' );
			                if ( 3 == $q->current_post ) {
				                the_widget( 'Nethr_Zena_Kolumna_Velika' );
			                }
		                }
		                wp_reset_postdata();
	                }
	                $feed = ob_get_clean();
	                wp_cache_set( 'nethr_zena_mobile_feed', $feed, 'nethr_mobile', 15 * MINUTE_IN_SECONDS );
                }
                echo $feed;
                ?>
            </section>

    </div>
<?php
get_footer();
