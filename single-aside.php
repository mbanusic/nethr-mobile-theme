<?php

get_header(); ?>

    <div class="container single full-container content">
        <?php
        while ( have_posts() ) {
            the_post();
            $titles = get_post_meta( get_the_ID(), 'extra_titles', true )
            ?>

            <div class="container content">

                <div class="article-content" id="__xclaimwords_wrapper">
                    <?php the_content(); ?>
                </div>

                <?php get_template_part('templates/share'); ?>


                <?php the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_footer' ) ) ?>

            </div>

        <?php } ?>

    </div>

<?php get_footer();