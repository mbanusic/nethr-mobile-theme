</div> <!-- END Content container -->

<?php dynamic_sidebar( 'sidebar-mobile-footer' ) ?>

<footer>
	<div class="wp-vip">
		<?php echo vip_powered_wpcom() ?>
	</div>

	<div class="bottom-icons cf">
		<?php global $wp;
		$current_url = trailingslashit( home_url( add_query_arg( array(), $wp->request ) ) ); ?>
		<a href="<?php echo esc_url( $current_url . '?ak_action=reject_mobile' ); ?>" class="desktop">
			<i class="fa fa-laptop"></i> Desktop
		</a>
		<a href="https://www.facebook.com/Net.hr" class="fb">
			<i class="fa fa-facebook"></i> Facebook
		</a>
		<a href="https://twitter.com/Nethr" class="tw">
			<i class="fa fa-twitter"></i> Twitter
		</a>
		<a href="mailto:vijesti@portal.net.hr?subject=Net.hr - kontakt" class="contact">
			<i class="fa fa-envelope-o"></i> Kontakt
		</a>
	</div>

</footer>

</div> <!-- END Main container -->


<?php wp_footer();
?>
<!-- AdSync -->
<script async src="https://cdn.adsync.tech/SpecialAdCampaigns/Index?portalWidgetId=137" type="text/javascript"></script>
<!-- AdSync -->
</body>
</html>