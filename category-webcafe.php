<?php
/* we only need this because parent has it :) */
get_header();

?>

	<div class="container full-container content">

		<?php

		if ( have_posts() ) {
			global $wp_query;
			while ( have_posts() ) {
				the_post();
				if ( 0 == $wp_query->current_post ) {
					get_template_part( 'templates/articles/article-1' );
				}
				else {
					get_template_part( 'templates/articles/article-3' );
				}
				if ( 4 == $wp_query->current_post ) {
					the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v1' ) );
				}
				if ( 10 == $wp_query->current_post ) {
					nethr_get_template( 'templates/articles/webcafe_article', array( 'category' => 'komnetar' ) ); // Komnetar
					nethr_get_template( 'templates/articles/webcafe_article', array( 'category' => 'cura-dana' ) ); // Cura dana
					nethr_get_template( 'templates/articles/webcafe_article', array( 'category' => 'decko-dana' ) ); // Decko dana
					nethr_get_template( 'templates/articles/webcafe_article', array( 'category' => 'fora-dana' ) ); // Fora dana
					nethr_get_template( 'templates/articles/webcafe_article', array( 'category' => 'overkloking' ) ); // Overkloking
				}
			}
		}

		?>
		<a class="load-more" href="<?php echo esc_url( get_next_posts_page_link() ) ?>">
			<i class="fa fa-align-justify"></i> Učitaj više
		</a>

		<?php
		the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_footer' ) ); ?>


	</div>

<?php get_footer();