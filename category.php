<?php

get_header();

?>

	<div class="container full-container content">
		<?php

		if ( have_posts() ) {
			global $wp_query;
			while ( have_posts() ) {
				the_post();
				if ( 0 == $wp_query->current_post ) {
					get_template_part( 'templates/articles/article-1' );
					the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_header' ) );
				}
				else {
					get_template_part( 'templates/articles/article-3' );
				}
				if ( 4 == $wp_query->current_post ) {
					the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v1' ) );
				}
			}
		}

		?>
		<a class="load-more" href="<?php echo esc_url( get_next_posts_page_link() ) ?>">
			<i class="fa fa-align-justify"></i> Učitaj više
		</a>

	</div>

<?php
the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_footer' ) );

get_footer();