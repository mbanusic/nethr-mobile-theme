<?php get_header(); ?>

	<?php the_widget( 'Nethr_Marquee_Widget' ); ?>

	<div class="container full-container content">

		<?php
		global $post;
		$q = z_get_zone_query( 'naslovnica-glavni', array( 'posts_per_page' => 11 ));
		while ( $q->have_posts() ) {
			$q->the_post();
			if ( 0 === $q->current_post ) {
				nethr_get_template( 'templates/articles/article-1', array('zone' => 'naslovnica-glavni', 'position' => $q->current_post+1) );
				the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_homepage_v1' ) );
			}
			else if ( 1 === $q->current_post || 2 === $q->current_post || 6 === $q->current_post || 7 === $q->current_post ) {
				nethr_get_template( 'templates/articles/article-2', array('zone' => 'naslovnica-glavni', 'position' => $q->current_post+1) );
			}
			else {
				nethr_get_template( 'templates/articles/article-3', array('zone' => 'naslovnica-glavni', 'position' => $q->current_post+1) );
			}
		}
		$promos = nethr_promos( 'mobile-feed' );
		if ( $promos && isset( $promos[0] ) && $promos[0] ) {
			$post = get_post( intval( $promos[0] ) );
			nethr_get_template( 'templates/articles/article-3', array('zone' => 'promo', 'position' => 1) );
		}
		if ( $promos && isset( $promos[1] ) && $promos[1] ) {
			$post = get_post( intval( $promos[1] ) );
			nethr_get_template( 'templates/articles/article-3', array('zone' => 'promo', 'position' => 2) );
		}
        ?>
		<?php the_widget('Nethr_Banner_Widget', ['size' => 'nethr_mobile_homepage_v2']) ?>
        <?php

		$q = z_get_zone_query( 'naslovnica-danas', array( 'posts_per_page' => 5 ));


		// Mobile sidebar
		dynamic_sidebar('mobile_sidebar');

		?>

        <?php the_widget('Nethr_Satovi_Widget'); ?>
	<div class="egida danas"><a href="<?php echo esc_url( site_url('kategorija/danas') ); ?>">Danas</a></div>
		<div class="big-block danas"><?php
		while ( $q->have_posts() ) {
			$q->the_post();
			if ( 0 === $q->current_post ) {
				nethr_get_template( 'templates/articles/article-1', array('zone' => 'naslovnica-danas', 'position' => $q->current_post+1) );
			}
			else if ( 1 === $q->current_post || 2 === $q->current_post || 6 === $q->current_post || 7 === $q->current_post ) {
				nethr_get_template( 'templates/articles/article-2', array('zone' => 'naslovnica-danas', 'position' => $q->current_post+1) );
			}
			else {
				nethr_get_template( 'templates/articles/article-3', array('zone' => 'naslovnica-danas', 'position' => $q->current_post+1) );
			}
		}
		?></div>
		<a class="load-more" href="<?php echo esc_url( site_url( 'kategorija/danas' ) ) ?>">
			<i class="fa fa-align-justify"></i> Učitaj više
		</a>
		<?php
		the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_homepage_v3' ) );

		$q = z_get_zone_query( 'naslovnica-sport', array( 'posts_per_page' => 5 ));
		?>
		<h2 class="section-title sport head">
			<a href="http://net.hr/kategorija/sport">
				Sport
			</a>
		</h2>
		<div class="big-block sport"><?php
		while ( $q->have_posts() ) {
			$q->the_post();
			if ( 0 === $q->current_post ) {
				nethr_get_template( 'templates/articles/article-1', array('zone' => 'naslovnica-sport', 'position' => $q->current_post+1) );
			}
			else if ( 1 === $q->current_post || 2 === $q->current_post || 6 === $q->current_post || 7 === $q->current_post ) {
				nethr_get_template( 'templates/articles/article-2', array('zone' => 'naslovnica-sport', 'position' => $q->current_post+1) );
			}
			else {
				nethr_get_template( 'templates/articles/article-3', array('zone' => 'naslovnica-sport', 'position' => $q->current_post+1) );
			}
		}
		?></div>
		<a class="load-more" href="<?php echo esc_url( site_url( 'kategorija/sport' ) ) ?>">
			<i class="fa fa-align-justify"></i> Učitaj više
		</a>
		<?php

		the_widget('Nethr_Super1_Mobile_Widget', array( 'zone' => '453' ));

		$q = z_get_zone_query( 'naslovnica-hot', array( 'posts_per_page' => 5 ));
		?>
		<div class="egida hot"><a href="<?php echo esc_url( site_url( 'kategorija/hot' ) ) ?>">Hot</a></div>
		<div class="big-block hot"><?php
			while ( $q->have_posts() ) {
				$q->the_post();
				if ( 0 === $q->current_post ) {
					nethr_get_template( 'templates/articles/article-1', array('zone' => 'naslovnica-hot', 'position' => $q->current_post+1) );
				}
				else if ( 1 === $q->current_post || 2 === $q->current_post || 6 === $q->current_post || 7 === $q->current_post ) {
					nethr_get_template( 'templates/articles/article-2', array('zone' => 'naslovnica-hot', 'position' => $q->current_post+1) );
				}
				else {
					nethr_get_template( 'templates/articles/article-3', array('zone' => 'naslovnica-hot', 'position' => $q->current_post+1) );
				}
			}
			?></div>
		<a class="load-more" href="<?php echo esc_url( site_url( 'kategorija/hot' ) ) ?>">
			<i class="fa fa-align-justify"></i> Učitaj više
		</a>
		<?php

		$q = z_get_zone_query( 'naslovnica-magazin', array( 'posts_per_page' => 5 ));
		?>
		<div class="egida magazin"><a href="<?php echo esc_url( site_url( 'kategorija/magazin' ) ) ?>">Magazin</a></div>
		<div class="big-block magazin"><?php
			while ( $q->have_posts() ) {
				$q->the_post();
				if ( 0 === $q->current_post ) {
					nethr_get_template( 'templates/articles/article-1', array('zone' => 'naslovnica-magazin', 'position' => $q->current_post+1) );
				}
				else if ( 1 === $q->current_post || 2 === $q->current_post || 6 === $q->current_post || 7 === $q->current_post ) {
					nethr_get_template( 'templates/articles/article-2', array('zone' => 'naslovnica-magazin', 'position' => $q->current_post+1) );
				}
				else {
					nethr_get_template( 'templates/articles/article-3', array('zone' => 'naslovnica-magazin', 'position' => $q->current_post+1) );
				}
			}
			?></div>
		<a class="load-more" href="<?php echo esc_url( site_url( 'kategorija/magazin' ) ) ?>">
			<i class="fa fa-align-justify"></i> Učitaj više
		</a>
		<?php

		$q = z_get_zone_query( 'naslovnica-webcafe', array( 'posts_per_page' => 5 ));
		?>

		<div class="egida webcafe"><a href="<?php echo esc_url( site_url( 'kategorija/webcafe' ) ) ?>">Webcafe</a></div>
		<div class="big-block webcafe"><?php
			while ( $q->have_posts() ) {
				$q->the_post();
				if ( 0 === $q->current_post ) {
					nethr_get_template( 'templates/articles/article-1', array('zone' => 'naslovnica-webcafe', 'position' => $q->current_post+1) );
				}
				else if ( 1 === $q->current_post || 2 === $q->current_post || 6 === $q->current_post || 7 === $q->current_post ) {
					nethr_get_template( 'templates/articles/article-2', array('zone' => 'naslovnica-webcafe', 'position' => $q->current_post+1) );
				}
				else {
					nethr_get_template( 'templates/articles/article-3', array('zone' => 'naslovnica-webcafe', 'position' => $q->current_post+1) );
				}
			}
			?></div>
		<a class="load-more" href="<?php echo esc_url( site_url( 'kategorija/webcafe' ) ) ?>">
			<i class="fa fa-align-justify"></i> Učitaj više
		</a>
	</div>

<?php
// Horoskop widget
the_widget('Nethr_Horoskop_Big');

get_footer();