<?php
get_header('ispovijesti');
?>

    <div class="container full-container content">
        <?php

        if ( have_posts() ) {
            global $wp_query;
            while ( have_posts() ) {
                the_post();
                if ( 5 == $wp_query->current_post ) {
                    the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v1' ) );
                }
	            ?>

                <article class="article-ispovijest cf">

                    <?php the_category(); ?>

                    <a href="<?php the_permalink(); ?>">
                        <h2 class="title"><?php the_title(); ?></h2>
                    </a>
                    <div class="ispovijesti-meta cf">
                        <span>
                            Korisnik: <?php echo esc_html( get_post_meta( get_the_ID(), 'autor', true ) ); ?>
                        </span>
                        <span>
                            Vrijeme: <?php the_time(); ?>
                        </span>
                        <span>
                            <i class="fa fa-comment"></i>
                            <block class="fb-comments-count" data-href="<?php the_permalink(); ?>"></block>
                        </span>
                    </div>
                </article>

	            <?php

            }
        }

        ?>
        <a class="load-more" href="<?php echo esc_url( get_next_posts_page_link() ) ?>">
            <i class="fa fa-align-justify"></i> Učitaj više
        </a>

    </div>

<?php dynamic_sidebar( 'ispovijesti_sidebar' ) ?>

<?php
the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_footer' ) );

get_footer();