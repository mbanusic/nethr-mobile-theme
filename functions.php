<?php

require_once( __DIR__ . '/../adriaticmedia-nethr/functions-shared.php' );

function nethr_scripts_mobile() {
	wp_enqueue_script( 'nethr-script-marquee', get_stylesheet_directory_uri() . '/js/marquee.min.js', array( 'jquery' ), '20141106', true );
    wp_enqueue_script('nethr-expand-banner', 'https://scripts.net.hr/dfp/expand.js', array( 'jquery' ), '20180820');
}

add_action( 'wp_enqueue_scripts', 'nethr_scripts_mobile' );

function nethr_load_style() {
	wp_enqueue_style( 'nethr-style', get_stylesheet_uri(), array(
		'nethr-font-awesome',
		'nethr-font-oswald',
		'nethr-font-source',
		'nethr-font-pt-sans',
		'nethr-font-pt-serif',
		'nethr-videojs'
	) );
}

add_action( 'wp_enqueue_scripts', 'nethr_load_style' );

// Injection of banner inside of article
add_filter( 'the_content', 'nethr_mobile_the_content' );

function nethr_mobile_the_content( $content ) {
	// check if password protected
	if ( post_password_required() ) {
		return $content;
	}
	if ( is_page() || 'webcafe' === get_post_type() ) {
		return $content;
	}
	// check if we don't have ads
	$oglasi = get_post_meta( get_the_ID(), 'oglasi', true );
	if ( isset( $oglasi['adocean'] ) && 1 === intval( $oglasi['adocean'] ) ) {
		return $content;
	}
	$new_line = "\n";
	$parts   = explode( $new_line, $content );
	$return_content = '';
	for ( $i = 0; $i <= count( $parts ); $i++ ) {
		$return_content .= $parts[$i] . $new_line;
		if ( 2 === $i ) {
			ob_start();
			the_widget( 'Nethr_Clickattack_Banner' );
			$ca = ob_get_clean();
			$return_content .= $ca . $new_line;
		}

		if ( 4 === $i ) {
			ob_start();
			the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v2' ) );
			$ad = ob_get_clean();
			$return_content .= $ad . $new_line;
		}

		if ( 9 === $i && count( $parts ) > 13 ) {
			ob_start();
			the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v3' ) );
			$ad = ob_get_clean();
			$return_content .= $ad . $new_line;
		}
	}

	ob_start();
	the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v1' ) );
	$adx1 = ob_get_clean();
	//append after lead
	$return_content = preg_replace( '/<\/h4>/', '</h4>'.$adx1, $return_content, 1);

	//append to end of content
	ob_start();
	the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v4' ) );
	$ad = ob_get_clean();
	$return_content .= $new_line . $ad;

	return $return_content;
}

add_filter( 'pre_get_posts', 'nethr_pre_get_posts' );

/**
 * @param WP_Query $query
 */
function nethr_pre_get_posts( $query ) {
	// affect only main query, other queries (e.g. in widgets) are specific and should not be filtered
	// we do not want to change anything in admin
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}

	$query->set( 'posts_per_page', 20 );
	if ( $query->is_post_type_archive( 'zena' ) || is_tax( 'zena-cat' ) ) {
		return $query;
	}
	if ( $query->is_post_type_archive( 'webcafe' ) ) {
		$query->set( 'post_type', array( 'post' ) );
		$query->set( 'category_name', 'webcafe' );
	}

	if ( is_archive() ) {
		$query->set( 'post_type', array( 'post', 'promo', 'ispovijesti', 'zena' ) );
		if ( is_category( 'komnetar' ) || is_category( 'cura-dana' ) || is_category( 'overkloking' ) || is_category( 'decko-dana' ) ) {
			$query->set( 'post_type', array( 'webcafe' ) );
		}
	}

}

add_filter('post_gallery', 'nethr_gallery_shortcode', 10, 3);

function nethr_gallery_shortcode( $output, $attr, $instance) {
	$attachments = get_posts( array( 'include' => $attr['ids'], 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'post__in' ) );
	ob_start();
	?>
	<div class="gallery" id="gallery-<?php echo intval( $instance ); ?>" data-slick='{"slidesToShow": 1, "slidesToScroll": 1}'>
		<?php
        $i = 1;
        foreach ($attachments as $post) {
            if ( 0 === $i % 5 ) {
                ?><div class="image banner banner-<?php echo esc_attr($i) ?>">

                </div><?php
	            $i++;
            }
			$image_output = wp_get_attachment_image( $post->ID, 'single-raw' );
			?><div class="image"><?php
			echo wp_kses_post( $image_output );
			?><span class="description"><?php echo esc_html( $post->post_excerpt ); ?></span> <span class="photographer"><?php echo esc_html( nethr_get_photographer( $post->ID ) ); ?></span></div><?php
	        $i++;
		} ?>
	</div>
	<script>
		jQuery('#gallery-<?php echo intval( $instance ); ?>').slick();
        jQuery('#gallery-<?php echo intval( $instance ); ?>').on('afterChange', function(event, slick, currentSlide, nextSlide){
            ga('newTracker.send', 'pageview', location.pathname);
            ga('oldTracker.send', 'pageview', location.pathname);
            pp_gemius_hit('15aQAmMQW9iFAGZeG8KJQKei.MhIw4xDi6L09YLL4Nr.q7');
            if ( 0 === (currentSlide+1) % 5 ) {
                getGalleryAd(currentSlide+1);
            }
        });
	</script>
	<?php
	return ob_get_clean();
}

add_action('nethr_body_start', 'nethr_cookie_disclaimer');

function nethr_cookie_disclaimer() {
    ?>
    <div class="disclaimer">
        <div class="container">
            <i class="fa fa-empire"></i>
            <div class="disclaimer-text">
                <div class="disclaimer-title">
                    Pravilnik upotrebi kolačića
                </div>
                Portal Net.hr unaprijedio je politiku privatnosti i korištenja takozvanih cookiesa, u skladu s novom europskom regulativom. Cookiese koristimo kako bismo mogli pružati našu online uslugu, analizirati korištenje sadržaja, nuditi oglašivačka rješenja, kao i za ostale funkcionalnosti koje ne bismo mogli pružati bez cookiesa. Daljnjim korištenjem ovog portala pristajete na korištenje cookiesa. Ovdje možete saznati više o <a href="https://net.hr/pravila-privatnosti/">zaštiti privatnosti</a> i <a href="https://net.hr/politika-o-cookiejima/">postavkama cookiesa</a>
            </div>
            <div class="disclaimer-btn">
                <a href="#" onclick="tmg_hide_disclaimer()" class="dsc-btn">Shvaćam</a>
            </div>

        </div>
        <style>
            .disclaimer {
                display:none;
                background: #f5f5f5;
                padding: 50px 0 20px;
                font-family: sans-serif;
                font-size: 15px;
            }

            .disclaimer a:not(.dsc-btn) {
                text-decoration: underline;
            }

            .disclaimer .container {
                position: relative;
            }

            .disclaimer i {
                position: absolute;
                top: 5px;
                font-size: 72px;
                left: -80px;
                color: #999;
            }

            .disclaimer-title {
                font-family: "Oswald", Impact, Sans-Serif;
                font-size: 24px;
                font-weight: normal;
                margin-bottom: 5px;
                color: #333;
            }

            .disclaimer-text {
                padding-right: 20px;
            }

            .disclaimer-btn {
                text-align: center;
                display: flex;
                flex-direction: column;
                justify-content: center;
                margin-top: 25px;
            }

            .dsc-btn {
                display: inline-block;
                text-transform: uppercase;
                font-family: "Oswald", Impact, Sans-Serif;
                font-size: 18px;
                background: #f15a22;
                color: white;
                padding: 6px 0;
                width: 150px;
            }

            .dsc-btn:hover {
                background: black;
            }
            .tmg-no-cookie .disclaimer {
                display: block;
            }
        </style>
        <script>
            function tmg_hide_disclaimer() {
                document.body.className = document.body.className.replace(/\btmg-no-cookie\b/,'');
                return false;
            }
            if (document.cookie.indexOf("tmg_visited=") >= 0) {
                tmg_hide_disclaimer();
            }
            else {
                // set a new cookie
                expiry = new Date();
                expiry.setTime(expiry.getTime()+(365*24*60*60*1000)); // 1 year

                // Date()'s toGMTSting() method will format the date correctly for a cookie
                document.cookie = "tmg_visited=yes; domain=.net.hr; path=/; expires=" + expiry.toGMTString();
            }
        </script>
    </div>
    <?php
}