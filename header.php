<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0px !important;">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.png">

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php do_action( 'nethr_body_start' ) ?>

<?php get_template_part( 'templates/navbar-2' ); ?>

<div class="main-container">

	<div class="content-container cf">

		<?php
		$show = false;
		if ( is_archive() ) {
			$show = true;
		}
		else if ( is_single() ) {
			global $post;
			$show = true;
			if ( 'webcafe' == $post->post_type ) {
				$cat = nethr_get_the_category();
				if ( 8216785 == $cat->parent ) { //do not show top category on horoscope
					$show = false;
				}
			}
		}

		if ( $show ) {
			$term_link = '';
			if ( is_category() || is_single() ) {
				$cat = nethr_get_top_category();
				$slug = $cat->slug;
				$name = $cat->name;
				$term_link = wpcom_vip_get_term_link( $cat, 'category' );
				if ( 439601120 == get_query_var( 'cat' ) ) {
					$slug = 'euro-2016';
				}
			}
			else {
				$term = get_queried_object();
				$slug = 'webcafe';
				$name = $term->name;
				$term_link = '#';
			}
			?>

		<?php if( 'euro-2016' == $slug ) {
		// Euro 2016 category ?>
			<h2 class="section-title euro-2016 head">
				<?php $image = get_option( 'nethr_euro_image_mobile' );
				if ( $image ) {
				?>
				<img src="<?php echo esc_url( $image ); ?>" width="265" height="45"/>
				<?php }
				else {
					echo 'EURO 2016';
				}
				?>
			</h2>
		<?php } else { ?>
			<h2 class="section-title head <?php echo esc_attr( $slug ); ?>">
				<i class="category-ico"></i>
				<?php
						if ( !$term_link || is_wp_error( $term_link ) ) {
							$term_link = '#'; //prevent catchable Fatal Errors when $term_link is a WP_Error
						}
				?>
				<a href="<?php echo esc_url( $term_link ); ?>">
					<?php echo esc_html( $name ); ?>
				</a>
			</h2>
		<?php } ?>
			<div class="underline"></div>
		<?php
		}
