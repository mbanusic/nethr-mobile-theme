<?php

get_header(); ?>

<div class="container single full-container content">
	<?php
	while ( have_posts() ) {
		the_post();
		$cat = nethr_get_the_category();
		?>

		<div class="featured-img"><?php the_post_thumbnail( 'single-raw' ); ?></div>
		<?php the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_header' ) ); ?>
		<div class="container content">
			<h2 class="overtitle <?php echo esc_html( $cat->slug ) ?>">
				<?php echo esc_html( $cat->name ); ?>
			</h2>

			<div class="article-bottom">
				<h1 class="title">
                    <?php
                    if ( 8216785 == $cat->parent ) { // we only show category name for horoscope categories
	                    $branding = intval( get_option( 'nethr_horoscope_branding', 0) );
	                    if ( 1 === $branding && 'dnevni-horoskop' === $parent->slug  ) {
		                    ?>
                            <a href="http://bit.ly/2hfNDAW" target="_blank">
                                <img
                                        style="width: 100%;"
                                        src="https://adriaticmedianethr.files.wordpress.com/2016/11/aurora_bigbill_custom.jpg?quality=100&strip=all"/>
                            </a>
                            <br>
		                    <?php
	                    }
	                    echo esc_html( $cat->name . ' ' );
                    }
                    the_title();
                    ?>
                </h1>
				<?php
				if ( ! has_post_thumbnail() ) {
					the_content();
				}
				?>
			</div>

		</div> <!-- end container -->

		<?php get_template_part('templates/share') ?>

		<div class="container">

			<?php the_widget('Nethr_Facebook_Comments_Widget') ?>

			<?php the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v1' ) ); ?>

		</div>

			<div class="more-articles">

				<?php 	$cat = nethr_get_the_category();
						$args = array(
							'posts_per_page' => 6,
							'category_name' => $cat->name,
							'post_type' => 'webcafe'
							);
					 	$articles = new WP_Query( $args );
						if ( $articles->have_posts() ) {
							while ( $articles->have_posts() ) {
								$articles->the_post();
								?>
								<article class="article-2 cf">
									<a href="<?php the_permalink(); ?>">
										<div class="article-text">
											<div class="thumb"><?php the_post_thumbnail( 'article-3_2x' ); ?></div>
											<h2><?php the_date(); ?></h2>
											<h3 class="title"><?php the_title(); ?></h3>
										</div>
									</a>
								</article>
						<?php }
				}
				wp_reset_postdata(); ?>
			</div>
	<?php } ?>

</div>

<?php
the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_footer' ) );

get_footer();