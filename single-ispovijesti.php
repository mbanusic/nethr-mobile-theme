<?php

get_header('ispovijesti'); ?>

    <div class="container single single-ispovijesti full-container content">
        <?php
        while ( have_posts() ) {
            the_post();
            ?>

            <div class="container content">

                <?php the_category(); ?>

                <h1 class="title"><?php the_title(); ?></h1>

                <div class="ispovijesti-meta">
                    <span>
                        Korisnik: <?php echo esc_html( get_post_meta( get_the_ID(), 'autor', true ) ) ?>
                    </span>
                    <span>
                        Vrijeme: <?php the_time(); ?>
                    </span>
                    <span>
                        <i class="fa fa-comment"></i>
                        <block class="fb-comments-count" data-href="<?php the_permalink(); ?>"></block>
                    </span>
                </div>


                <div class="article-content" id="__xclaimwords_wrapper">
                    <?php the_content(); ?>
                </div>

                <?php get_template_part( 'templates/share' ); ?>

                <?php the_widget('Nethr_Facebook_Comments_Widget') ?>

            </div>

        <?php } ?>

        <?php the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_footer' ) ) ?>

    </div>

    <?php dynamic_sidebar( 'ispovijesti_sidebar' ) ?>

<?php get_template_part( 'templates/single-categories' ); ?>

<?php get_footer();