<?php

get_header(); ?>

    <div class="container single full-container content">
        <?php
        while ( have_posts() ) {
            the_post();
            $titles = get_post_meta( get_the_ID(), 'extra_titles', true )
            ?>

            <div
                class="featured-img"><?php the_post_thumbnail( 'article-1' ); ?></div>
            <div class="container content">
                <?php $cat = nethr_get_the_category(); ?>
                <h2 class="overtitle <?php echo esc_html( $cat->slug ) ?>">
                    <?php
                    if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
                        echo esc_html( $titles['over_title'] );
                    }
                    else {
                        echo esc_html( $cat->name );
                    }
                    ?>
                </h2>

                <h1 class="title"><?php the_title(); ?></h1>

                <div class="article-content">
                    <?php the_content(); ?>
                </div>
	            <div id="__xclaimwords_wrapper"></div>

                <?php get_template_part('templates/share'); ?>

                <?php the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_footer' ) ) ?>

            </div>

        <?php } ?>

    </div>

<?php get_footer();