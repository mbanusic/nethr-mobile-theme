<?php
get_header('zena-net');
?>
    <div class="container category cf">

        <div class="page-grid">

            <?php
            //main feed
            if ( have_posts() ) {
                while ( have_posts() ) {
                    the_post();
                    if ( 0 == $wp_query->current_post ) {
                        get_template_part( 'templates/articles/article-1' );
                    } else {
                        get_template_part( 'templates/articles/article-3' );
                    }
                }
                wp_reset_postdata();
            }
            ?>

            <div class="article-navigation">
                <?php next_posts_link( 'JOŠ VIJESTI <i class="fa fa-angle-right"></i>' ); ?>
            </div>

        </div>


    </div>


<?php
get_footer();
