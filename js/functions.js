jQuery(document).ready(function () {
    mobileMenu();
    ticker();
    horoscope();
    scroll_home();
    fb_shares();
    webshop();
    track_outbound_link();
});

function ticker() {
    jQuery(".ticker ul").ticker({effect: "slideUp"});
    jQuery('.marquee').marquee({
        startVisible: true
    });
}

function webshop() {
    jQuery('.webshop-widget .widget-body').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });
}

function mobileMenu() {
    var scrolled;
    jQuery(document).scroll(function() {
        var y = jQuery(this).scrollTop()

        if (y > 45) {
            jQuery('header').addClass('fix');
        } else {
            jQuery('header').removeClass('fix');
        }
    });

    jQuery('#menu-toggle').click( function() {
        if ( jQuery('body').hasClass('toggled-on') )
        {
            jQuery('body').removeClass('toggled-on');
            jQuery('.main-container').css( 'top', '' );
            jQuery(document).scrollTop(scrolled);
        } else {
            scrolled = jQuery(document).scrollTop();
            jQuery('body').addClass('toggled-on');
            jQuery('.main-container').css( 'top', -scrolled +'px' );
        }

    });

    jQuery('#search-activator').click( function() {
        jQuery('#google-search').fadeIn(200);
    });

    jQuery('#google-search i').click( function() {
        jQuery('#google-search').fadeOut(200);
    });

    jQuery("#toTop").click(function () {
        jQuery("html, body").animate({scrollTop: 0}, 500);
    });


}

function horoscope() {
    //Horoskop
    var item = jQuery('.horoskop .item:first-child'),
        up_arrow = jQuery('.horoskop-arrow.up'),
        down_arrow = jQuery('.horoskop-arrow.down');
    var margin = item.css('margin-top');
    if ( margin === '0px' ) {
        up_arrow.hide();
    }
    var click = 0;
    up_arrow.click( function(){
        click++;
        if (click == 1) {
            click = -12;
        }
        item.css('margin-top', click*200 + 'px');
        margin = item.css('margin-top');
        if ( click == 0 ) {
            jQuery(this).hide();
        }
        if ( click == -10 ) {
            down_arrow.show();
        }
    });
    down_arrow.click( function(){
        click--;
        if (click == -12) {
            click = 0;
        }
        item.css('margin-top', click*200+'px');
        if ( click == -11 ) {
            jQuery(this).hide();
        }
        if ( click == -1 ) {
            up_arrow.show();
        }
    });
}

function scroll_home() {
    if (!jQuery('body').hasClass('home')) {
        return;
    }
    var klasa = '';
    var old_klasa = '';
    var egidas = jQuery('.egida');
    var navbar = jQuery('.main-navbar');
    jQuery(window).scroll(function() {
        klasa = '';
        jQuery.each(egidas, function(index, value) {
            var top = value.getBoundingClientRect().top;
            if (top < 50) {
                klasa = jQuery(value).attr('class').split(' ');
                klasa = klasa[1];
            }
        });
        if (klasa) {
            if (!navbar.hasClass('colored')) {
                navbar.addClass('colored');
            }
            if (klasa != old_klasa) {
                navbar.addClass(klasa);
                if (old_klasa) {
                    navbar.removeClass(old_klasa);
                }
                old_klasa = klasa;
            }
        }
        else {
            if (navbar.hasClass('colored')){
                navbar.removeClass('colored');
                if (old_klasa) {
                    navbar.removeClass(old_klasa);
                    old_klasa = '';
                }
            }
        }
    });
}

function fb_shares() {
    jQuery('.fb-share-count').each(function() {
        var href = jQuery(this).data('href'), that = this;
        jQuery.get('https://graph.facebook.com/v2.6/?fields=share{share_count}&id='+href, function(r) {
            if (typeof r.share.share_count !== "undefined") {
                jQuery(that).text(r.share.share_count);
            }
        });
    });
}

function getGalleryAd(image) {
    if ( 0 !== jQuery(".banner-"+image).children().length ) {
        return;
    }
    var id = image / 5;
    var div = jQuery( '<div />');
    div.attr('data-glade', true);
    div.attr('data-ad-unit-path', '/88449691/net.hr_300x250v' + encodeURIComponent(id) );
    div.css({
        position: 'relative',
        width: '300px',
        height: '250px'
    });
    var script = jQuery('<script />');
    script.attr('src', 'https://securepubads.g.doubleclick.net/static/glade.js');
    script.attr('async', true);
    jQuery(".banner-" + image).append(div);
    jQuery(".banner-" + image).append(script);
}

function track_outbound_link() {
    jQuery('a[target=_blank]').click(function() {
        var url = jQuery(this).attr('href');
        ga('newTracker.send', 'event', 'outbound', 'click', url, {
            'transport': 'beacon'
        });
        ga('oldTracker.send', 'event', 'outbound', 'click', url, {
            'transport': 'beacon'
        });
    });

    jQuery('#naslovnica-danas-7').click(function() {
        var url = jQuery(this).attr('href');
        ga('newTracker.send', 'event', 'crosslink', 'tap', url, {
            'transport': 'beacon'
        });
        ga('oldTracker.send', 'event', 'crosslink', 'tap', url, {
            'transport': 'beacon'
        });
    })
}

/* LAZY LOAD ADS */
function lazy_ad_load (id, offset) {
    if (show_var === undefined) {
        var show_var = [];
    }
    if(jQuery("#" + id).length){
        show_var[id]=0;
        jQuery(window).scroll(function() {
            if (show_var[id] == 0 && jQuery(window).scrollTop() > jQuery("#"+id).offset().top-offset) {
                var append_content = '<script>googletag.cmd.push(function() { googletag.display("' + id + '"); });</script>';
                jQuery("#"+id).append(append_content);
                show_var[id]=1;
            }
        });
    }
}
/* END LAZY LOAD ADS */


