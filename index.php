<?php

get_header();
?>

	<div class="container full-container content">

		<div class="najnovije-head">
			<h2 class="section-title">Najnovije vijesti <div class="arrow"></div></h2>
		</div>

		<?php
		global $wp_query;
		while ( have_posts() ) {
			the_post();
			get_template_part( 'templates/articles/article-3' );
			if ( 5 == $wp_query->current_post ) {
				the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_intext_v1' ) );
			}
		}

		?>

		<a class="load-more" href="<?php echo esc_url( get_next_posts_page_link() ) ?>">
			<i class="fa fa-align-justify"></i> Učitaj više
		</a>

	</div>

<?php
the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_footer' ) );

get_footer();