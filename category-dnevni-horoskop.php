<?php
// NET.HR -> Category
get_header();
$cat_id = get_query_var( 'cat' );
$category = get_category( $cat_id );
?>
    <div class="container category cf">

        <div class="section-title">
            <?php
            $category_link = wpcom_vip_get_term_link( $category, 'category' );
            echo esc_html( $category->name );
            ?>
        </div>

        <div class="page-grid cf">
            <?php
            $branding = intval( get_option( 'nethr_horoscope_branding', 0) );
            if ( 1 === $branding && 'dnevni-horoskop' === $parent->slug  ) { ?>
            <br>
            <a href="http://bit.ly/2hfNDAW" target="_blank">
                <img
                    style="width: 100%;"
                    src="https://adriaticmedianethr.files.wordpress.com/2016/11/aurora_bigbill_custom.jpg?quality=100&strip=all" />
            </a>
            <?php } ?>
            <div class="horoskop-grid grid-4 cf">
                <?php
                $terms = get_terms( 'category', array( 'parent' => $category->term_id ) );

                foreach( $terms as $cat ) {

                    $args = array(
                        'post_type' => array( 'webcafe' ),
                        'cat'  => $cat->term_id,
                        'posts_per_page' => 1
                    );
                    $q = new WP_Query( $args );
                    if ( $q->have_posts() ) {
                        while ( $q->have_posts() ) {
                            $q->the_post(); ?>

                            <article
                                class="grid-article dnevni-horoskop <?php echo esc_html( $cat->slug ); ?>">
                                <div class="inner">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="thumb"></div>
                                        <h2 class="title"><?php echo esc_html( $cat->name ); ?></h2>
                                    </a>
                                </div>
                            </article>

                        <?php }
                        wp_reset_postdata();
                    }
                }  ?>
            </div>
        </div>


        <div class="sidebar single-sidebar single-sidebar-1">
            <?php dynamic_sidebar( 'sidebar-webcafe' ) ?>
        </div>

    </div>


<?php
get_footer();
