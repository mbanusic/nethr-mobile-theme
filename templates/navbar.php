<?php
$header_klasa = 'home';
$header_link  = home_url();
$header_text  = 'Net.hr';
if ( is_single() || is_category() ) {
	$is_home      = false;
	$header_klasa = 'category';
	if ( is_single() ) {
		$cat = nethr_get_the_category();
	} else {
		$cat = get_category( get_query_var( 'cat' ) );
	}

	if ( 'crna-kronika' != $cat->slug ) {
		while ( $cat->parent ) {
			$cat = get_category( $cat->parent );
		}
	}
	$header_klasa .= ' ' . $cat->slug;
}
?>


<header class="<?php echo esc_html( $header_klasa ); ?>">

	<div class="main-navbar">
		<div class="container cf">

			<div id="navbar-left"></div>
			<a href="<?php echo esc_url( $header_link ) ?>">
				<div class="logo">
					<h1><?php echo esc_html( $header_text ) ?></h1>

					<div class="logo-bg-tip"></div>
				</div>
			</a>

			<div class="sponsor">
				<?php get_template_part( 'templates/sponsor' ); ?>
			</div>

			<i class="fa fa-bars" id="menu-activator" data-atribute="off"></i>

		</div>
	</div>

	<div class="main-menu cf">
		<?php wp_nav_menu( array( 'menu' => 'Mobilni' ) ); ?>
		<form id="google-search" action="<?php echo esc_url( site_url('pretrazivanje') ); ?>">
			<input type="hidden" name="cx" value="partner-pub-2317149376955370:2370221132" />
			<input type="hidden" name="cof" value="FORID:10" />
			<input type="hidden" name="ie" value="UTF-8" />
			<i class="fa fa-search"></i>
			<input type="text" name="q" size="31" placeholder="Google pretraživanje" />

		</form>
	</div>

</header>