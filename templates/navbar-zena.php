<header class="zena">

    <div class="main-navbar zena <?php if( is_single() || is_category() || is_tax( 'zena-cat' ) ) { echo ' colored'; } ?>">
        <div class="container cf">

            <div id="menu-toggle" class="menu-toggle">
                <div class="burger-before"></div>
                <div class="burger"></div>
                <div class="burger-after"></div>
            </div>

	        <a href="http://net.hr/zena/">

            <div class="logo">
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/zenanet_logo.svg" height="47px"/>
            </div>
            </a>

            <i class="fa fa-search" id="search-activator" data-atribute="off"></i>

        </div>
    </div>

    <div class="side-menu">
        <h3>Rubrike</h3>
        <?php wp_nav_menu( array( 'menu' => 'Žena Net' ) ); ?>
        <h3>Servisi</h3>
        <?php wp_nav_menu( array( 'menu' => 'Servisni' ) ); ?>
    </div>

    <form id="google-search" action="<?php echo esc_url( site_url( 'pretrazivanje' ) ); ?>">
        <input type="hidden" name="cx" value="partner-pub-2317149376955370:2370221132" />
        <input type="hidden" name="cof" value="FORID:10" />
        <input type="hidden" name="ie" value="UTF-8" />
        <input type="text" name="q" size="31" placeholder="Google pretraživanje" />
        <i class="fa fa-close"></i>
    </form>

</header>