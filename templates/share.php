<div class="share cf">

	<a href="https://www.facebook.com/dialog/share?app_id=103100136402693&display=touch&href=<?php echo rawurlencode( get_the_permalink() ); ?>&redirect_uri=<?php echo rawurlencode( get_the_permalink() ); ?>" class="fb fatty"><i class="fa fa-facebook"></i>&nbsp;&nbsp;Podijeli</a>
	<a href="https://twitter.com/intent/tweet?counturl=<?php echo rawurlencode( get_the_permalink() ) ?>&text=<?php echo rawurlencode( get_the_title() ); ?>&url=<?php echo rawurlencode( get_the_permalink() ) ?>&via=Nethr" target="_blank" class="tw"><i class="fa fa-twitter"></i></a>
	<a href="whatsapp://send?text=<?php echo rawurlencode( get_the_title() . ' ' . get_the_permalink() ) ?>" class="wa" data-action="share/whatsapp/share"><i class="fa fa-whatsapp"></i></a>
	<a href="#comment" class="co"><i class="fa fa-comment-o"></i></a>

</div>