<?php $category = $options['category'] ?>
<div class="category-head <?php echo esc_html( $category ); ?> cf">
	<a href="<?php echo esc_url( get_term_link( $category, 'category' ) ); ?>">
		<h2 class="category-title"><?php echo esc_html( $category ); ?></h2>
		<span class="more">Još vijesti <i class="fa fa-angle-right"></i></span>
	</a>
</div>

<?php // Article 1 -> Latest articles

$args = array( 'posts_per_page' => 4, 'category_name' => $category );
$articles = new WP_Query( $args );
if ( $articles->have_posts() ) {
	while ( $articles->have_posts() ) {
		$articles->the_post();
		if ( $articles->current_post == 0 ) {
			get_template_part( 'templates/articles/article-1' );
		}
		else {
			get_template_part( 'templates/articles/article-2' );
		}

	}
}
wp_reset_postdata();
?>

<a class="load-more <?php echo esc_html( $category ); ?>"
   href="<?php echo esc_url( get_term_link( $category, 'category' ) ); ?>">
	<i class="fa fa-align-justify"></i> Učitaj više
</a>
