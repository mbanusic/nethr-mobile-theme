<a href="<?php the_permalink(); ?>">
	<article class="article-2 komnetar cf">
		<div class="article-text">
			<div class="thumb"><?php the_post_thumbnail( 'article-3' ); ?></div>
			<h2 class="overtitle">
				<?php
				$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
				if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
					echo esc_html( $titles['over_title'] );
				} else {
					$value = nethr_get_the_category();
					echo esc_html( $value->name );
				}
				?>
			</h2>

			<h1 class="title"><?php the_title(); ?></h1>
		</div>
	</article>
</a>