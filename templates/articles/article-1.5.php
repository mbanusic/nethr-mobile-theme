<a href="<?php the_permalink(); ?>" target="<?php echo esc_html( nethr_permalink_target() ) ?>">
    <article class="article-1-5 cf">
        <div class="article-text">
            <?php $cat = nethr_get_the_category( get_the_ID() ); ?>
            <div
                class="thumb"><?php the_post_thumbnail( 'article-2' ); ?></div>
            <h2 class="overtitle <?php echo esc_html( nethr_category_color() ) ?>">
                <?php
                $titles = get_post_meta( get_the_ID(), 'extra_titles', true );
                if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
                    echo esc_html( $titles['over_title'] );
                } else {
                    echo esc_html( $cat->name );
                }
                ?>
            </h2>

            <h1 class="title"><?php the_title(); ?></h1>
        </div>
    </article>
</a>