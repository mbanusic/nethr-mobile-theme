<a href="<?php the_permalink(); ?>" target="<?php echo esc_html( nethr_permalink_target() ) ?>"
	<?php if (isset($options['zone']) && isset($options['position']) && $options['zone'] && $options['position']) {
		echo 'id="' . $options['zone'] . '-' . $options['position'] . '"';
	}  ?>
>
	<article class="article-1 <?php echo esc_html( nethr_category_color() ) ?>">

		<div class="article-text">
			<?php $cat = nethr_get_the_category( get_the_ID() ); ?>
			<h2 class="overtitle <?php echo esc_html( nethr_category_color() ) ?>">
				<?php
				$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
				if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
					echo esc_html( $titles['over_title'] );
				}
				else {
					echo esc_html( $cat->name );
				}
				?>
			</h2>

			<h1 class="title"><?php the_title(); ?></h1>
		</div>
		<div class="bg-shadow"></div>
		<?php
		if( 'zena' == get_post_type() ) {
			the_post_thumbnail( 'single-raw' );
		} else {
			the_post_thumbnail( 'article-1' );
		}
		?>

		<?php
		// Custom sticker: Lokalni izbori 2017
		if( has_term('lokalni-izbori-2017', 'post_tag') ) {
            ?><img class="custom-sticker" src="<?php echo esc_url( get_template_directory_uri() . '/img/lok_izb_2017.svg' ) ?>" width="90" height="90" /><?php
		} ?>

	</article>
</a>