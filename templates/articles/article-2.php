<article class="article-2 cf <?php echo esc_html( nethr_category_color() ) ?>">
	<a href="<?php the_permalink(); ?>" target="<?php echo esc_html( nethr_permalink_target() ) ?>"
		<?php if (isset($options['zone']) && isset($options['position']) && $options['zone'] && $options['position']) {
			echo 'id="' . $options['zone'] . '-' . $options['position'] . '"';
		}  ?>
    >
		<div class="article-text">
			<?php $cat = nethr_get_the_category( get_the_ID() ); ?>
			<div class="thumb">
				<?php
				if( 'zena' == get_post_type() ) {
					the_post_thumbnail( 'article-1' );
				} else {
					the_post_thumbnail( 'article-3_2x' );
				}

				?>
			</div>
			<h2 class="overtitle">
				<?php
				$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
				if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
					echo esc_html( $titles['over_title'] );
				} else {
					echo esc_html( $cat->name );
				}
				?>
			</h2>

			<h1 class="title"><?php the_title(); ?></h1>
		</div>
	</a>
</article>