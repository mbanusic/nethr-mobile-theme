<div class="foto-vijesti">

	<a class="section-title" href="#">Foto Vijesti</a>

	<div class="underline"></div>
	<div class="section-body cf">

		<?php
		$args     = array(
			'posts_per_page' => 3,
			'offset'    => 0,
			'tax_query' => array(
				array(
					'taxonomy' => 'post_format',
					'field'    => 'slug',
					'terms'    => array( 'post-format-gallery' )
				),
			)
		);
		$articles = new WP_Query( $args );
		if ( $articles->have_posts() ) {
			while ( $articles->have_posts() ) {
				$articles->the_post(); ?>

				<article class="foto-article">
					<a href="<?php the_permalink(); ?>">
						<?php the_post_thumbnail( 'article-3_2x' );
						$titles = get_post_meta( get_the_ID(), 'extra_titles', true );
						if ( isset( $titles['short_title'] ) && $titles['short_title'] ) {
							echo esc_html( $titles['short_title'] );
						} else {
							the_title();
						} ?>
					</a>
				</article>
			<?php }
		}
		wp_reset_postdata(); ?>

	</div>
</div>