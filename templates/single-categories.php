<?php
$data = wp_cache_get( 'nethr_mobile_read_more', 'nethr_mobile' );
if ( !$data ) {
	ob_start();

// Danas
	$q = z_get_zone_query( 'naslovnica-danas', array( 'posts_per_page' => 5 ) );
	?>
	<div class="egida danas"><a
		href="<?php echo esc_url( site_url( 'danas' ) ) ?>">Vijesti</a>
	</div><?php
	while ( $q->have_posts() ) {
		$q->the_post();
		if ( 0 == $q->current_post ) {
			get_template_part( 'templates/articles/article-1' );
		}
		else if ( 1 == $q->current_post || 2 == $q->current_post ) {
			get_template_part( 'templates/articles/article-2' );
		}
		else {
			get_template_part( 'templates/articles/article-3' );
		}
	}

// Sport
	$q = z_get_zone_query( 'naslovnica-sport', array( 'posts_per_page' => 5 ) );
	?>

	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<!-- Mobile footer 2 300x250 -->
	<ins class="adsbygoogle"
	     style="display:block"
	     data-ad-client="ca-pub-2317149376955370"
	     data-ad-slot="8321216732"
	     data-ad-format="auto"></ins>
	<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
	</script>

	<div class="egida sport"><a
		href="<?php echo esc_url( site_url( 'sport' ) ) ?>">Sport</a></div><?php
	while ( $q->have_posts() ) {
		$q->the_post();
		if ( 0 == $q->current_post ) {
			get_template_part( 'templates/articles/article-1' );
		}
		else if ( 1 == $q->current_post || 2 == $q->current_post ) {
			get_template_part( 'templates/articles/article-2' );
		}
		else {
			get_template_part( 'templates/articles/article-3' );
		}
	}

	// Hot
	$q = z_get_zone_query( 'naslovnica-hot', array( 'posts_per_page' => 5 ) );
	?>
	<div class="egida hot"><a href="<?php echo esc_url( site_url( 'hot' ) ) ?>">Hot</a>
	</div><?php
	while ( $q->have_posts() ) {
		$q->the_post();
		if ( 0 == $q->current_post ) {
			get_template_part( 'templates/articles/article-1' );
		}
		else if ( 1 == $q->current_post || 2 == $q->current_post ) {
			get_template_part( 'templates/articles/article-2' );
		}
		else {
			get_template_part( 'templates/articles/article-3' );
		}
	}

// Magazin
	$q = z_get_zone_query( 'naslovnica-magazin', array( 'posts_per_page' => 5 ) );
	?>
	<div class="egida magazin"><a
		href="<?php echo esc_url( site_url( 'magazin' ) ) ?>">Magazin</a>
	</div><?php
	while ( $q->have_posts() ) {
		$q->the_post();
		if ( 0 == $q->current_post ) {
			get_template_part( 'templates/articles/article-1' );
		}
		else if ( 1 == $q->current_post || 2 == $q->current_post ) {
			get_template_part( 'templates/articles/article-2' );
		}
		else {
			get_template_part( 'templates/articles/article-3' );
		}
	}

	// Webcafe
	$q = z_get_zone_query( 'naslovnica-webcafe', array( 'posts_per_page' => 5 ) );
	?>
    <div id='nethr_mobile_bf_300x250_v2'><script> googletag.cmd.push(function() { googletag.display('nethr_mobile_bf_300x250_v2');});</script></div>

    <div class="egida webcafe"><a href="<?php echo esc_url( site_url( 'webcafe' ) ) ?>">Webcafe</a>
	</div><?php
	while ( $q->have_posts() ) {
		$q->the_post();
		if ( 0 == $q->current_post ) {
			get_template_part( 'templates/articles/article-1' );
		}
		else if ( 1 == $q->current_post || 2 == $q->current_post ) {
			get_template_part( 'templates/articles/article-2' );
		}
		else {
			get_template_part( 'templates/articles/article-3' );
		}
	}


	$data = ob_get_clean();
	wp_cache_set( 'nethr_mobile_read_more', $data, 'nethr_mobile', 30 * MINUTE_IN_SECONDS );
}
echo $data;