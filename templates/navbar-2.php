<?php
$header_klasa = 'home';
$header_link  = home_url();
if ( is_single() || is_category() ) {
    $is_home      = false;
    $header_klasa = 'category';
    if ( is_single() ) {
        $cat = nethr_get_the_category();
    } else {
        $cat = get_category( get_query_var( 'cat' ) );
    }

    if ( 'crna-kronika' != $cat->slug ) {
        while ( $cat->parent ) {
            $cat = get_category( $cat->parent );
        }
    }
    $header_klasa .= ' ' . $cat->slug;
}
?>


<header class="<?php echo esc_html( $header_klasa ); ?>">

    <div class="main-navbar
    <?php
    echo esc_html( $header_klasa );
    if( is_single() || is_category() ) { echo ' colored'; } ?>">
        <div class="container cf">

            <div id="menu-toggle" class="menu-toggle">
                <div class="burger-before"></div>
                <div class="burger"></div>
                <div class="burger-after"></div>
            </div>

                <?php if( is_front_page() ) {
                    echo '<a id="toTop">';
                } else {
                    echo '<a href="' . esc_url($header_link) . '">';
                }?>
                <div class="logo">
                    <h1>Net.hr</h1>
                </div>
            </a>

            <i class="fa fa-search" id="search-activator" data-atribute="off"></i>

        </div>
    </div>

    <div class="side-menu">
        <h3>Rubrike</h3>
        <?php wp_nav_menu( array( 'menu' => 'Mobilni' ) ); ?>
        <h3>Servisi</h3>
        <?php wp_nav_menu( array( 'menu' => 'Servisni' ) ); ?>
    </div>

        <form id="google-search" action="<?php echo esc_url( site_url('pretrazivanje') ); ?>">
            <input type="hidden" name="cx" value="partner-pub-2317149376955370:2370221132" />
            <input type="hidden" name="cof" value="FORID:10" />
            <input type="hidden" name="ie" value="UTF-8" />
            <input type="text" name="q" size="31" placeholder="Google pretraživanje" />
            <i class="fa fa-close"></i>
        </form>

</header>