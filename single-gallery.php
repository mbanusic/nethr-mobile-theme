<?php

get_header(); ?>

	<div class="container gallery full-container content">
		<?php
		while ( have_posts() ) {
			the_post();
			?>

			<div id="gallery-title"><?php the_title(); ?></div>

			<div class="article-content">
				<?php the_content(); ?>
			</div>

		<?php } ?>

        <?php get_template_part('templates/share'); ?>

	</div>

<?php
the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_footer' ) );
get_footer();