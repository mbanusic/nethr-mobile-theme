<?php // Page pretrazivanje
get_header(); ?>

<div class="search-head">
	<div class="search-container container cf">

		<form class="searchbar" action="<?php echo esc_url( site_url( 'pretrazivanje' ) ); ?>" id="cse-search-box">
            <input type="hidden" name="cx" value="partner-pub-2317149376955370:2370221132" />
            <input type="hidden" name="cof" value="FORID:10" />
            <input type="hidden" name="ie" value="UTF-8" />
            <input type="text" name="q" size="31" value="<?php if ( isset($_GET['q'] ) ) echo esc_html( $_GET['q'] ); ?>" />
            <i class="fa fa-search"></i>
		</form>
		<script type="text/javascript" src="//www.google.hr/coop/cse/brand?form=cse-search-box&amp;lang=hr"></script>

	</div>
</div>

<div class="search-body">
	<div class="search-container cf">
		<script>
			(function() {
				var cx = 'partner-pub-2317149376955370:2370221132';
				var gcse = document.createElement('script');
				gcse.type = 'text/javascript';
				gcse.async = true;
				gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
				'//cse.google.com/cse.js?cx=' + cx;
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(gcse, s);
			})();
		</script>
		<gcse:searchresults-only defaultToRefinement="net.hr"></gcse:searchresults-only>
	</div>
</div>
	<div id="__xclaimwords_wrapper"></div>

<?php get_footer(); ?>