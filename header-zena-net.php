<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0px !important;">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.png">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php do_action( 'nethr_body_start' ) ?>

<?php get_template_part( 'templates/navbar-zena' ); ?>

<div class="main-container zena">

    <div class="content-container cf">
