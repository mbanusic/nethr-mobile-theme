<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top: 0px !important;">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/favicon.png">

    <?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php do_action( 'nethr_body_start' ) ?>

<div class="main-container ispovijesti">

    <div class="header-ispovijesti" style="background-image: url( '<?php echo esc_url( get_option( 'nethr_ispovijesti_header', '' ) ) ?>' );">
        <a href="<?php echo esc_url( site_url( 'kategorija/ispovijesti' ) ) ?>">
            <img src="https://adriaticmedianethr.files.wordpress.com/2016/06/ispovijesti_logo22.png" width="260">
        </a>
    </div>

    <div class="content-container cf">

        <?php
        $show = false;

        if ( is_archive() ) {
            $show = true;
        }
        else if ( is_single() ) {
            global $post;
            $show = true;
        }

        if ( $show ) {
        $term_link = '';
        if ( is_category() || is_single() ) {
            $cat = nethr_get_top_category();
            $slug = $cat->slug;
            $name = $cat->name;
            $term_link = wpcom_vip_get_term_link( $cat, 'category' );
        }
        else {
            $term = get_queried_object();
            $slug = 'webcafe';
            $name = $term->name;
            $term_link = '#';
        }
        ?>

        <?php } ?>

        <div class="ispovijesti-buttons">
            <a href="http://net.hr/">
                <i class="fa fa-long-arrow-left"></i> Natrag na Net.hr
            </a>
            <a href="<?php echo esc_url( site_url('ostavi-ispovijest') ); ?>">
                <i class="fa fa-edit"></i> Ostavi ispovijest
            </a>
        </div>

        <div class="ispovijesti-count">
            <?php
            $cat = false;
            if ( is_single() ) {
                $cat = get_the_category();
                if ( $cat && !is_wp_error( $cat ) && is_array( $cat ) ) {
                    $cat = $cat[0]; //Ispovijesti will only have one category
                }
            }
            else if ( is_category() ) {
                $cat = get_category( get_query_var( 'cat' ) );
            }
            if ( $cat && !is_wp_error( $cat ) ) {
                if ( 'ispovijesti' == $cat->slug ) {
                    $post_count = wp_count_posts( 'ispovijesti' );
	                $post_count = $post_count->publish;
                }
                else {
                    $post_count = $cat->category_count;
                }
                if ( $post_count ) {
                    ?>
                    Trenutno imamo <span
                        class="highlight"><?php echo esc_html( $post_count ); ?>
                    ispovijesti </span><?php

                    if ( $cat && ! is_wp_error( $cat ) && 'ispovijesti' != $cat->slug  ) {
                        echo esc_html( 'u kategoriji ' . $cat->name );
                    }
                    ?>
                <?php }
            }?>
        </div>

        <?php
        wp_nav_menu( array(
            'menu' => 'Ispovijesti',
         ) );
        ?>
