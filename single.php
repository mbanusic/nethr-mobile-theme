<?php

get_header(); ?>

<div class="container single full-container content">
	<?php
	while ( have_posts() ) {
		the_post();
		$titles = get_post_meta( get_the_ID(), 'extra_titles', true )
		?>

		<div class="featured-img">
			<?php
			// Custom sticker: Lokalni izbori 2017
			if( has_term('lokalni-izbori-2017', 'post_tag') ) {
				?><img class="custom-sticker" src="<?php echo esc_url( get_template_directory_uri() . '/img/lok_izb_2017.svg' ) ?>" width="90" height="90" /><?php
			}
			// Post thumbnail
			the_post_thumbnail( 'article-1' ); ?>
		</div>
		<div class="container content">
			<?php $cat = nethr_get_the_category(); ?>
			<h2 class="overtitle <?php echo esc_html( $cat->slug ) ?>">
				<?php
				if ( isset( $titles['over_title'] ) && $titles['over_title'] ) {
					echo esc_html( $titles['over_title'] );
				}
				else {
					echo esc_html( $cat->name );
				}
				?>
			</h2>
			<?php
			the_widget( 'Nethr_Banner_Widget', array( 'size' => 'nethr_mobile_header' ) );
			?>
			<h1 class="title"><?php the_title(); ?></h1>

            <div class="author">Autor: <?php coauthors(); ?> </div>

			<div class="article-content" id="__xclaimwords_wrapper">
				<?php the_content(); ?>
				<div class="tags">
					<?php the_tags( '', ' ', '' ); ?>
				</div>
			</div>

			<?php get_template_part( 'templates/share' ); ?>
			<?php dynamic_sidebar( 'sidebar-mobile' ) ?>
		</div>

	<?php } ?>

</div>

<?php get_template_part( 'templates/single-categories' ); ?>

<?php get_footer();